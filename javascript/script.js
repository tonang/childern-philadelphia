(function ($) {

	'use strict';

    //Variables
	var winWidth = $(window).width();
	

	// PRELOADER
	$(window).bind( "load" , function() { // make sure the whole site is loaded
		$("#status").fadeOut(); // will first fade out the loading animation
		$("#preloader").delay(450).fadeOut("slow"); // will fade out the white DIV that covers the website.

	});

	// FOR CLONE TO NAVIGATION-MAIN
	$(".navigation-main > ul").clone(false).find("ul,li").removeAttr("id").remove(".sub-menu").appendTo($("#nav-side-left> .append-nav"));

	// FOR CLONE TO NAV-MEGA
	// ACTIVE SIDEBAR ON LEFT
	$('.btn-show, .btn-hide').click( function() {
		var nav_side = $("#nav-side-left");
		var wrapper = $(".wrapper-inner");
		nav_side.toggleClass("active-nav");
		wrapper.toggleClass("on-active");
	});

	// NAVIGATION-MAIN
	$('.navigation-main ul').superfish({
		delay: 400,
		animation: {
			opacity: 'show',
			height: 'show'
		},
		animationOut: {
			opacity: 'hide',
			height: 'hide'
		},
		speed: 200,
		speedOut: 200,
		autoArrows: false
	});

	// CSF-CAROUSEL
	$('.csf-carousel').owlCarousel({
		animateOut: 'fadeOut',
		// animateIn: 'flipInX',
		items: 1,
		// margin: 30,
		// stagePadding: 30,
	    autoplay:false,
	    autoplayTimeout:3000,
	    loop: true,
		smartSpeed: 100,
		// dots: true,
		// nav: true
	});

	// TAX-CAROUSEL
	$('.tax-carousel').owlCarousel({
		items: 3,
		nav: true,
		margin: 20,
		navContainer: '.tax-nav',
		navText: [
				"<i class='fa fa-angle-left fa-2x'></i>",
            	"<i class='fa fa-angle-right fa-2x'></i>"
		],
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			450: {
				items: 2
			},
			1000:{
				items: 3
			},
		}
	});

	//YOUNG FRIENDS OF CSFP-CAROUSEL
	$('.young-carousel').owlCarousel({
		animateOut: 'fadeOut',
		items: 1,
		nav: true,
		navText: [
				"<i class='left fa fa-angle-left fa-2x'></i>",
            	"<i class='right fa fa-angle-right fa-2x'></i>"
		],
	    autoplay: true,
	    autoplayTimeout: 3000,
	    loop: true,
		smartSpeed: 1000
	});

	//Partner on Landing Page - CAROUSEL
	$('.partners-carousel').owlCarousel({
		animateOut: 'fadeOut',
		items: 1,
		nav: true,
		navText: [
				"<i class='left fa fa-angle-left fa-2x'></i>",
            	"<i class='right fa fa-angle-right fa-2x'></i>"
		],
	    autoplay: true,
	    autoplayTimeout: 3000,
	    loop: true,
		smartSpeed: 1000,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			450: {
				items: 3
			},
			1000:{
				items: 6
			},
		}
	});

	var heightCreditImg = $(".img-copy-height img");
	var heightCreditDesc = $(".container-oto");
	function heightCredit(){
		heightCreditDesc.height( $(heightCreditImg).height() );
	}
	heightCredit();

	var heightSiteContent = $(".site-content");
	var heightSiteSidebar = $(".site-nav-sidebar");
	function heightSidebar(){
		if(winWidth > 992){
			heightSiteSidebar.height( $(heightSiteContent).height() + 50);
		}
	}
	heightSidebar();

	// GET VALUE FROM MENU-YEARS
	$('.menu-years li a').click( function() {
		var value = $(this).text();
		$(this).parents('.btn-group').find('.dropdown-toggle').html(value+ ' <i class="fa fa-chevron-down"></i>');
	});

	// VIDEO-CONTAINER
	$(".video-container").fitVids();

	var widthContainer = $(".container");
	var countNavigation = $(".navigation-main ul:first-child").children().length;

	var widthList = $(".navigation-main ul:first-child>li");
	function widthNavigation(){
		widthList.width( $(widthContainer).width()/$(countNavigation) );
		console.log( $(widthContainer).width() );
		console.log( $(countNavigation) );
		console.log(widthList.width() );
	}
	widthNavigation();

	//imgLiquid
	 $(".imgLiquidFill").imgLiquid({
        fill: true,
        horizontalAlign: "center",
        verticalAlign: "top"
    });
})(jQuery); 

! function(a) {
    "use strict";

    function b() {
        g.height(a(f).height())
    }

    function c() {
        e > 992 && i.height(a(h).height() + 50)
    }

    function d() {
        l.width(a(j).width() / a(k)), console.log(a(j).width()), console.log(a(k)), console.log(l.width())
    }
    var e = a(window).width();
    a(window).bind("load", function() {
        a("#status").fadeOut(), a("#preloader").delay(450).fadeOut("slow")
    }), a(".navigation-main > ul").clone(!1).find("ul,li").removeAttr("id").remove(".sub-menu").appendTo(a("#nav-side-left> .append-nav")), a(".btn-show, .btn-hide").click(function() {
        var b = a("#nav-side-left"),
            c = a(".wrapper-inner");
            b.toggleClass("active-nav");
            c.toggleClass("on-active");
    }), a(".navigation-main ul").superfish({
        delay: 400,
        animation: {
            opacity: "show",
            height: "show"
        },
        animationOut: {
            opacity: "hide",
            height: "hide"
        },
        speed: 200,
        speedOut: 200,
        autoArrows: !1
    }), a(".csf-carousel").owlCarousel({
        animateOut: "fadeOut",
        items: 1,
        autoplay: !1,
        autoplayTimeout: 3e3,
        loop: !0,
        smartSpeed: 100
    }), a(".tax-carousel").owlCarousel({
        items: 3,
        nav: !0,
        margin: 20,
        navContainer: ".tax-nav",
        navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
        responsiveClass: !0,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 2
            },
            1e3: {
                items: 3
            }
        }
    }), a(".fullslider-carousel").owlCarousel({
        items: 7,
        autoplay: !0,
        autoplayTimeout: 1e3,
        autoplayHoverPause: !0,
        nav: !1,
        margin: 0,
        autoWidth: !0,
        touchDrag: !0,
        responsiveClass: !0,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 4
            },
            1e3: {
                items: 7
            }
        }
    }), a(".partners-carousel").owlCarousel({
        items: 6,
        autoplay: !1,
        autoplayTimeout: 1e3,
        autoplayHoverPause: !0,
        nav: !0,
        margin: 0,
        autoWidth: !0,
        navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
        responsiveClass: !0,
        responsive: {
            0: {
                items: 1
            },
            450: {
                items: 4
            },
            1100: {
                items: 5
            },
            1200: {
                items: 7
            }
        }
    });
    var f = a(".img-copy-height img"),
        g = a(".container-oto");
    b();
    var h = a(".site-content"),
        i = a(".site-nav-sidebar");
    c(), a(".menu-years li a").click(function() {
        var b = a(this).text();
        a(this).parents(".btn-group").find(".dropdown-toggle").html(b + ' <i class="fa fa-chevron-down"></i>')
    }), a(".video-container").fitVids();
    var j = a(".container"),
        k = a(".navigation-main ul:first-child").children().length,
        l = a(".navigation-main ul:first-child>li");
    d()
}(jQuery),
function(a) {
    a.fn.countTo = function(b) {
        b = a.extend({}, a.fn.countTo.defaults, b || {});
        var c = Math.ceil(b.speed / b.refreshInterval),
            d = (b.to - b.from) / c;
        return a(this).each(function() {
            function e() {
                h += d, g++, a(f).html(h.toFixed(b.decimals).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")), "function" == typeof b.onUpdate && b.onUpdate.call(f, h), g >= c && (clearInterval(i), h = b.to, "function" == typeof b.onComplete && b.onComplete.call(f, h))
            }
            var f = this,
                g = 0,
                h = b.from,
                i = setInterval(e, b.refreshInterval)
        })
    }, a.fn.countTo.defaults = {
        from: 0,
        to: 100,
        speed: 1e3,
        refreshInterval: 100,
        decimals: 0,
        onUpdate: null,
        onComplete: null
    }
}(jQuery), jQuery(function(a) {
    var b, c = function(b) {
            a(b).each(function() {
                var c = parseFloat(a(b).attr("data-from"));
                return c
            })
        },
        d = function(b) {
            var c = parseFloat(a(b).attr("data-to"));
            return c
        },
        e = [".timer1", ".timer2", ".timer3", ".timer4", ".timer5", ".timer6", ".timer7"];
    for (b = 0; b < e.length; b++) console.log(e[b]), a(e[b]).countTo({
        from: c(e[b]),
        to: d(e[b]),
        speed: 6e3,
        refreshInterval: 50,
        onComplete: function() {
            console.debug(this)
        }
    })
});